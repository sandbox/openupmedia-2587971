INTRODUCTION
------------

This module provides a block with a dropdown containing the links towards the
translations of the current page.


REQUIREMENTS
------------

This module requires the following modules:
 * Internationalization (https://www.drupal.org/project/i18n)


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.


CONFIGURATION
-------------

After enabling the module a new "Language dropdown" block will become available.
You can place it on the website using the default Drupal blocks configuration
page, Context, Panels or any other module providing configuration for displaying
blocks.


MAINTAINERS
-----------

Current maintainers:
 * El Phaenax (elphaenax) - https://www.drupal.org/u/elphaenax

The project has been created at and sponsored by:
 * Open Up Media
   Open Up Media is a full service brand agency, based in Antwerp, Belgium. The
   agency originated from the merger between Savant Media and Apluz. Both
   agencies brought their years of experience and expertise together to offer
   customers a wider range of services.

   We have been developing websites almost exclusively using Drupal since 2007.
   We have a complimentary team that delivers beautiful, user-friendly, and
   rock-solid websites. Visit https://www.openupmedia.be for more information.
