/**
 * @file
 * Adds the Language Dropdown Block javascript.
 */

(function ($) {
  'use strict';

  /**
   * Adds the modal functionality.
   */
  Drupal.behaviors.LanguageDropdownBlock = {
    attach: function (context, settings) {
      var $language_selection = $('.language-dropdown');
      var $toggler = $language_selection.find('.language-toggler');
      $toggler.data('wrapper', $language_selection);

      // Add the functionality to show the external links.
      $toggler.click(function (e) {
        e.preventDefault();
        var $this = $(this);
        if ($this.data('wrapper').hasClass('show')) {
          hideLanguageDropdown();
        }
        else {
          showLanguageDropdown();
        }
      });

      /**
       * Helper function to show the language navigation.
       */
      var showLanguageDropdown = function () {
        $language_selection.addClass('show');
        $language_selection.removeClass('hide');
      };

      /**
       * Helper function to hide the language navigation.
       */
      var hideLanguageDropdown = function () {
        $language_selection.removeClass('show');
        $language_selection.addClass('hide');
      };
    }
  };
})(jQuery);
