<?php
/**
 * @file
 * The main template for the Language Dropdown module.
 */
?>
<div class="language-dropdown">
  <a href="#" class="language-toggler"><span><?php print strtoupper(i18n_langcode()); ?></span></a>
  <nav class="clearfix">
    <?php print $language_list; ?>
  </nav><!-- /.clearfix -->
</div><!-- /.language-dropdown -->
